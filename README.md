# Dependencies #

```
#!bash

 sudo pip install Flask
```


# How to run #

```
#!bash

 nohup python remote.py
```

import telnetlib
import socket

from time import sleep

class PioneerAvClientException(Exception):
    pass

class PioneerAvClient(object):
    "Telnet client to Pioneer VSX"""

    def __init__(self, ip, port=8102):
        self.ip = ip
        self.port = port
    
    def sendcmd(self, cmd):
        "Sends single command to AV"""
        try:
            sleep(0.2)
            print "connecting to " + self.ip + ":" + str(self.port)
            self.tn = telnetlib.Telnet(self.ip, self.port,1)
            self.tn.read_eager()
            print "connected to device"
            self.tn.write(cmd + '\r\n')
            print "send: "+ cmd.replace('\r\n', '')
            response = self.tn.read_until('\r\n',2).replace('\r\n', '');
            print "recv: " + response
            print "disconnected from device"
            self.tn.close()
            return response
        except socket.timeout:
            raise PioneerAvClientException("error connecting to device")        

#!/usr/bin/env python
import sys
import os
import re

from pioneer import PioneerAvClient

from flask import Flask
from flask import render_template
from flask import request, redirect, url_for

app = Flask(__name__)

pioneer = PioneerAvClient('10.0.1.133')

@app.route("/")

def index():
    return render_template('control.html')

@app.route("/click/<op>")
def clicked(op=None):
    # Send message to Lirc to control the IR
    func = re.sub(r'KEY_(.*)',r'\1',op)
    return getattr(sys.modules[__name__], func)()  

@app.route("/vol")
def vol():
    return pioneer.sendcmd("?V").replace('VOL','')

def PS3():
    return pioneer.sendcmd("25FN")

def TV():
    return pioneer.sendcmd("05FN")

def APPLE():
    return pioneer.sendcmd("19FN")

def TUNER():
    return pioneer.sendcmd("02FN")
    
def VOLUMEUP():
    return pioneer.sendcmd("VU")

def VOLUMEDOWN():
    return pioneer.sendcmd("VD")

def MUTEON():
    return pioneer.sendcmd("MO")

def MUTEOFF():
    return pioneer.sendcmd("MF")
    
def UP():
    return "UP"

def DOWN():
    return "DOWN"

def LEFT():
    return "LEFT"

def RIGHT():
    return "RIGHT"

def OK():
    return "OK"
    
def TV_POWER():
    return "TV_POWER"

def POWER():
    if ( pioneer.sendcmd("?P") == "PWR0" ):
        return pioneer.sendcmd("PF")
    else:
        return pioneer.sendcmd("PO")

def TV_INPUT():
    return "TV_INPUT"

def RECV():
    return "RECV"

if __name__ == "__main__":
    app.debug = True
    app.run('0.0.0.0')




var event;
var attendQuery;

$(document).ready(function () {
    //docReady();
	updateVolume();
});

function updateVolume() {
    $.ajax({url: '/vol', dataType: 'text'}).done(function(data) {
		if (data === 'MUT1') updateVolume();
		$('#volume_level').text(data);
    });
}

function clickedOp(op) {
    $.ajax({
		url: '/click/' + op,
		dataType: 'text'
	})
	.done(function(data) {
		console.log(data);
		if (op === 'KEY_VOLUMEUP' || op === 'KEY_VOLUMEDOWN')
			updateVolume();
    })
	.fail(function(jqXHR, textStatus, errorThrown) {
		$('#volume_level').text(textStatus);
	});
}

